extends RigidBody2D

func _ready() -> void:
	pass

remote func set_ball_position(new_position : Vector2):
	if get_tree().get_rpc_sender_id() != 1:
		return

	position = new_position