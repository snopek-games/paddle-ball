extends Node2D

var player_speed := 100

var ball_start_direction := 0
var ball_speed := 40
var ball_scene = preload("res://Ball.tscn")
var ball: RigidBody2D

var my_player: KinematicBody2D
var players := []
var game_started := false

var server_only := false
var using_agones := false

onready var connect_screen := $'%ConnectScreen'
onready var hud := $'%HUD'

func _ready() -> void:
	get_tree().connect("network_peer_connected", self, "_on_peer_connected")
	get_tree().connect("network_peer_disconnected", self, "_on_peer_disconnected")

	var arguments = get_commandline_args()

	if OS.has_feature("Server") and not arguments.has("serve"):
		print ("Must pass --serve=PORT to start the server")
		get_tree().quit(1)

	print (arguments)

	if arguments.has('serve'):
		var port = arguments['serve']
		server_only = true
		_on_ConnectScreen_serve(int(port))

		if arguments.has('agones'):
			using_agones = true
			AgonesSDK.start()
			AgonesSDK.ready()

			var agones_timer := Timer.new()
			agones_timer.name = 'AgonesTimer'
			agones_timer.wait_time = 2.0
			add_child(agones_timer)
			agones_timer.connect("timeout", self, "_on_AgonesTimer_timeout")
			agones_timer.start()
	else:
		connect_screen.visible = true

func get_commandline_args() -> Array:
	var arguments = {}
	for argument in OS.get_cmdline_args():
		# Parse valid command-line arguments into a dictionary
		if argument.find("=") > -1:
			var key_value = argument.split("=")
			arguments[key_value[0].lstrip("--")] = key_value[1]
		else:
			arguments[argument.lstrip("--")] = true
	return arguments

func create_ball() -> void:
	if ball:
		remove_child(ball)
		ball.queue_free()

	ball = ball_scene.instance()
	ball.name = 'Ball'
	ball.position = $BallStartPosition.position
	add_child(ball)

func start_ball() -> void:
	create_ball()

	var ball_angle := (randi() % 90) + 135
	var ball_vector := Vector2(1, 0)

	ball_vector = ball_vector.rotated(deg2rad(ball_angle))
	if ball_start_direction:
		ball_vector = ball_vector.rotated(deg2rad(180))
	ball_start_direction = !ball_start_direction;

	ball.apply_central_impulse(ball_vector * ball_speed)

func _physics_process(delta: float) -> void:
	if my_player and not server_only:
		var vector = Vector2()
		if Input.is_action_pressed("player_up"):
			vector.y += -1
		if Input.is_action_pressed("player_down"):
			vector.y += 1

		if get_tree().is_network_server():
			server_move_paddle(my_player.name, vector)
		else:
			rpc_id(1, "server_move_paddle", my_player.name, vector)

	if ball and get_tree().is_network_server():
		ball.rpc("set_ball_position", ball.position)

remote func server_move_paddle(node_name: String, input_vector: Vector2) -> void:
	# We only want this RPC running on the network server.
	if not get_tree().is_network_server():
		return

	# We only allow controlling these nodes.
	if not node_name in ['Player1', 'Player2']:
		return

	var player = get_node(node_name)
	player.move_paddle(input_vector * player_speed * get_physics_process_delta_time())

func _on_Player2ScoreArea_body_entered(area: Area2D) -> void:
	if get_tree().is_network_server():
		hud.rpc("increment_score", 0, 1)
		start_ball()

func _on_Player1ScoreArea_body_entered(area: Area2D) -> void:
	if get_tree().is_network_server():
		hud.rpc("increment_score", 1, 0)
		start_ball()

remote func set_my_player(name) -> void:
	if get_tree().get_rpc_sender_id() != 1:
		return
	my_player = get_node(name)

func _on_ConnectScreen_serve(port : int) -> void:
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(port, 2 if server_only else 1)
	get_tree().set_network_peer(peer)

	connect_screen.visible = false

	var msg = "Listening on " + str(port)
	hud.show_message(msg)
	print(msg)

	if not server_only:
		my_player = $Player1

func _on_ConnectScreen_connect(host, port) -> void:
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(host, port)
	get_tree().set_network_peer(peer)

	connect_screen.visible = false

	hud.show_message("Connecting...")

func _on_peer_connected(peer_id: int):
	hud.hide_message()

	if get_tree().is_network_server():
		if using_agones:
			# If adding the first player, mark this server as allocated.
			if players.size() == 0:
				AgonesSDK.allocate()

		players.append(peer_id)

		if server_only:
			rpc_id(peer_id, "set_my_player", "Player" + str(players.size()))
			if players.size() == 2:
				start_ball()
		else:
			rpc_id(peer_id, "set_my_player", "Player2")
			start_ball()
	else:
		create_ball()
		ball.mode = RigidBody2D.MODE_KINEMATIC

func _on_peer_disconnected(peer_id: int):
	if get_tree().is_network_server():
		players.erase(peer_id)

		if using_agones:
			if game_started:
				AgonesSDK.shutdown()
				yield (AgonesSDK, "agones_response")
				get_tree().quit()
			elif players.size() == 0:
				AgonesSDK.ready()

func _on_AgonesTimer_timeout() -> void:
	AgonesSDK.health()
