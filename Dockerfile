
#####

FROM barichello/godot-ci:3.5 AS app_builder

COPY . /app

WORKDIR /app

RUN godot --export-pack "Linux/X11" /app/paddle-ball.pck

#####

FROM ubuntu:20.04 AS app_runtime

RUN apt-get update && apt-get install -y \
    apt-transport-https \
    ca-certificates \
 && rm -rf /var/lib/apt/lists/*

ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

RUN mkdir /app \
 && useradd -ms /bin/bash app

COPY --from=app_builder /app/paddle-ball.pck /app/paddle-ball.pck
COPY --from=app_builder /usr/local/bin/godot /app/paddle-ball

USER app
WORKDIR /app

CMD ["/app/paddle-ball", "--serve=30000", "--agones"]

EXPOSE 30000/udp

