extends KinematicBody2D

func move_paddle(vector : Vector2):
	move_and_collide(vector)
	rpc("set_paddle_position", position)

remote func set_paddle_position(new_pos : Vector2):
	if get_tree().get_rpc_sender_id() != 1:
		return

	position = new_pos