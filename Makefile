
BUILD_TAG?=local
PUSH_TAG?=latest

build:
	docker build -t registry.gitlab.com/snopek-games/paddle-ball:$(BUILD_TAG) .

push: build
	docker tag registry.gitlab.com/snopek-games/paddle-ball:$(BUILD_TAG) registry.gitlab.com/snopek-games/paddle-ball:$(PUSH_TAG)
	docker push registry.gitlab.com/snopek-games/paddle-ball:$(PUSH_TAG)

.PHONY: build push

